module Main where

import Xmobar.JH.Config (config)
import Xmobar (xmobar)

main :: IO ()
main = xmobar config
