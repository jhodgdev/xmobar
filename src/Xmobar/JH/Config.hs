module Xmobar.JH.Config (config) where

import Xmobar
  ( Config (commands, font, overrideRedirect, position, template),
    Date (Date),
    Runnable (Run),
    XMonadLog (XMonadLog),
    XPosition (TopH),
    defaultConfig,
  )

config :: Config
config =
  defaultConfig
    { overrideRedirect = False,
      font = "Iosevka Nerd Font SemiBold 16",
      position = TopH 35,
      commands =
        [ Run $ Date "%a %Y-%m-%d * %H%M" "theDate" 10,
          Run XMonadLog
        ],
      template =
        "%XMonadLog% }{ "
          ++ "%theDate%"
    }
