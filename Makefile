##
# Project Title
#
# @file
# @version 0.1

install:
	cabal install \
		--flags="all_extensions" \
		--enable-executable-stripping \
		--enable-optimization=2 \
		--installdir=$$HOME/.local/bin \
		--overwrite-policy=always

build:
	cabal build \
		--flags="all_extensions"



# end
